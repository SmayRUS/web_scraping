import requests
import re
import json
from bs4 import BeautifulSoup
from sys import argv

def main():
    #Получение данных с консоли
    script, month, year = argv
    
    #C помощью fake_useragent генерируем рандомного пользователя.
    #Это сделано для избежания подозрений сайта, что мы бот.
    #Сгенерированные данные заносим в header.
    user = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
    header = {
        'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
        'User-Agent': user
    }

    #Основной сайт для скрепинга
    url = 'https://tenders.mts.ru/'
    url_add = 'default.aspx'

    #Cоздаем сессию и отправляем GET запрос на страницу
    #с используемым header.
    sess = requests.session()
    responce = sess.get(f'{url}{url_add}', headers=header).text

    #Полученные данные мы начинаем парсить для формирования POST запроса.
    #__VIEWSTATE является динамическим значением и нам требуется его
    #обновлять каждый раз когда загружаем новую страничку.
    soup = BeautifulSoup(responce, 'lxml')
    VIEWSTATE = soup.find('input',{'id':'__VIEWSTATE'}).get('value')
    VIEWSTATEGENERATOR = soup.find('input',{'id':'__VIEWSTATEGENERATOR'}).get('value')
    SM_TSM = soup.find('input',{'id':'ctl00_scriptManager_TSM'}).get('value')

    #Объявляем словарь data для отправки POST запроса.
    data = {
        'ctl00$scriptManager': f'ctl00$ctl00$ContentPlaceHolderBody$mainPanelPanel|ctl00$ContentPlaceHolderBody$calendarNavigation${year}',
        'ctl00_scriptManager_TSM': SM_TSM,
        '__EVENTTARGET': f'ctl00$ContentPlaceHolderBody$calendarNavigation${year}',
        '__EVENTARGUMENT': '',
        '__VIEWSTATE': VIEWSTATE,
        '__VIEWSTATEGENERATOR': VIEWSTATEGENERATOR,
        '__VIEWSTATEENCRYPTED': '',
        '__ASYNCPOST': 'true',
        'RadAJAXControlID': 'ctl00_radAjaxManager'
    }

    #Отправка POST запроса для перемещения на нужный год.
    #Обернуто в функцию в случае повторного вызова
    res = post_responce(sess, f'{url}{url_add}', data, header)
    print("Первый POST запрос успешно отправлен")

    #Полученный сайт имеет иную структуру.
    #Искать новое состояние __VIEWSTATE 
    #не имеет смысла через BeautifulSoup.
    #В функции dynamic_state мы реализуем поиск через регулярные выражения.
    
    upgrade = dynamic_state(res, year, month)

    #Обновляем словарь отправки данных.
    data.update(upgrade)

    #Отправка POST запроса для перемещения на нужный месяц.
    res = post_responce(sess, f'{url}{url_add}', data, header)
    print("Второй POST запрос успешно отправлен")

    #Основная функция сбора данных.
    save_tenders = scraping(sess, res, url, header)

    #Сохраняем в JSON формате.
    save_JSON(save_tenders)

    #Выход и завершения процессов.
    sess.close()
    exit()

def post_responce(sess, url, data, header):
    return sess.post(url, data=data, headers=header).text

def dynamic_state(res, year, month):
    parser_split = re.split(r'__VIEWSTATE[|]', res)
    result = re.split(r'[|]', parser_split[1])
    VIEWSTATE = result[0]
    
    #Найденное состояние мы вносим в словарь.
    #Также обновляем поля для перехода на нужный нам месяц.
    upgrade = {
        'ctl00$scriptManager': f'ctl00$ctl00$ContentPlaceHolderBody$mainPanelPanel|ctl00$ContentPlaceHolderBody$calendarNavigation${year}_{month}',
        '__EVENTTARGET': f'ctl00$ContentPlaceHolderBody$calendarNavigation${year}_{month}',
        '__VIEWSTATE': VIEWSTATE
    }
    print("Новый __VIEWSTATE найден.")
    return upgrade

def scraping(sess, res, url, header):
    #Объявляем список сохраненной информации тендеров для последующего формирования данных в JSON
    save_tenders = []

    #Парсим сайт для нахождения всех адресов тендеров.
    soup = BeautifulSoup(res, 'lxml')
    block = soup.find('div', id = 'ctl00_ContentPlaceHolderBody_mainPanel')
    purchases_links = block.find_all('div', class_ = 'm-purchases-list-row')

    print(f"Найдено {len(purchases_links)} тендеров...")
    #Проходим по каждой ссылке и находим нужную нам информацию.
    for i, link in enumerate(purchases_links):
        #Объявляем словарь для записи основной информации. 
        #Объявления списка для найденных приложенных документов.
        tend_dict = {}
        file_list = []

        #Заносим в словарь название ссылки.
        #Через регулярные выражения удаляем скрытые символы и мусор.
        tend_dict["Название"] = re.sub('[\t\r\n]|[\"]', '', link.find('a').text)

        #Находим адрес страницы
        tender_link = link.find('a').get('href')

        #Переходим через GET запрос на страничку тендера
        res = sess.get(f'{url}{tender_link}', headers=header).text
        
        #Находим текстовый блок со всей информацией
        soup = BeautifulSoup(res, 'lxml')
        block_tender = soup.find('div', id = 'ctl00_ContentPlaceHolderBody_divSummary')
        
        #Проходим по каждому элементу <p>, поскольку найти
        #id или class не представляется возможным.
        for p in block_tender.find_all('p'):
            #Получаем текстовую информацию абзаца.
            text = p.get_text()

            #Через условия мы находим нужные поля и заносим их в словарь.
            if re.search(r'Номер торгов:', text):
                if len(re.split(r': ', text)) <= 1:
                    tend_dict["Номер торгов"] = 'none'
                else:
                    tend_dict["Номер торгов"] = re.split(r': ', text)[1]
            elif re.search(r'Дата окончания:', text):
                if len(re.split(r': ', text)) <= 1:
                    tend_dict["Дата окончания"] = 'none'
                else:
                    tend_dict["Дата окончания"] = re.split(r': ', text)[1]
            elif re.search(r'Ответственное лицо:', text):
                if len(re.split(r': ', text)) <= 1:
                    tend_dict["Ответственное лицо"] = 'none'
                else:
                    tend_dict["Ответственное лицо"] = re.split(r': ', text)[1]
            elif re.search(r'Дата публикации на сайте:', text):
                if len(re.split(r': ', text)) <= 1:
                    tend_dict["Дата публикации на сайте"] = 'none'
                else:
                    tend_dict["Дата публикации на сайте"] = re.split(r': ', text)[1]
        
        #Находим если блок приложенных документов
        #если он существует.
        doc_link = soup.find('div', id = 'ctl00_ContentPlaceHolderBody_FilesPanel')

        #Если находим блок документов, то проходим по каждой ссылке,
        #записываем названия в отдельный словарь и список.
        if doc_link != None:
            for a in doc_link.find_all('a'):
                file_dict = {}
                text = a.get_text()
                find_href = a.get('href')
                file_dict["name"] = re.sub('[\t\r\n]', '', text)
                file_dict["href"] = f'{url}{find_href}'
                file_list.append(file_dict)

            #Заносим данные в основной словарь.
            tend_dict["files"] = file_list
        
        #Сохраняем данные в список.
        save_tenders.append(tend_dict)
        i += 1
        print(f"Открытие тендера №{i} прошло успешно.")

    print("Данные успешно сформированы.")
    return save_tenders

def save_JSON(save_tenders):
    with open('data.json', 'w', encoding='utf-8') as f:
        json.dump(save_tenders, f, indent=2, ensure_ascii=False)
    print("Данные успешно сохранены в JSON формат.")

main()
