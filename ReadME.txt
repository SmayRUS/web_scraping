Реализованн Scraper скрипт.
Данный скрипт находит все тендеры за указанный месяц и год.
В качестве информации собирает: 
- Название
- Номер торгов
- Ответственное лицо
- Дата публикации на сайте
- Дата окончания
а также приложенные документы если они есть.
Полученные данные формируются в JSON формат.
Код прокоменнтирован для обьяснения его работы.
Также добавлены try, except в случае выхода из строя скрипта.

Использованные библиотеки:
-requests (для передачи запроса на страничку сайта)
-BeautifulSoup (для парсинга страницы)
-lxml (дополнение для BeautifulSoup)
-fake_useragent (для генерированния пользователя для обхода защиты на проверку бота)
-re (регулярные выражения для расширения поиска)
-sys argv (для ввода к комадную строку данных)
-json (для формирования данных в JSON формат)